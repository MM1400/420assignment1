from flask import Flask, request, render_template
from math import sqrt
from random import randint

app = Flask(__name__)

@app.route("/")
def home():
    return render_template('exercises.html')

@app.route("/exercise1")
def exercise1():
    return render_template('exercise1.html')

@app.route("/exercise2")
def exercise2():
    return render_template('exercise2.html')

@app.route("/exercise3")
def exercise3():
    return render_template('exercise3.html')

@app.route("/exercise4")
def exercise4():
    return render_template('exercise4.html')

@app.route("/exercise5")
def exercise5():
    return render_template('exercise5.html')

@app.route('/exercise1', methods=['POST'])
def exercise1_post():
    dividend = int(request.form['dividend'])
    divisor = int(request.form['divisor'])
    message = ""
    if dividend % 2 == 0:
        message = message + f"{dividend} is even.\n"
    else:
        message = message + f"{dividend} is odd.\n"
    if dividend % 4 == 0:
        message = message + f"{dividend} is multiple of 4.\n"
    if dividend % divisor == 0:
        message = message + (f"{dividend} is a multiple of {divisor}.\n")
    return message

@app.route('/exercise2', methods=['POST'])
def exercise2_post():
    name = request.form['name']
    grade = float(request.form['grade'])
    if grade > 100 or grade < 0:
        return f"wrong grade {name}: {grade}"
    if grade >= 95:
        return f"your grade is {grade}, {name}--> you get A"
    elif grade >= 80:
        return f"your grade is {grade}, {name}--> you get B"
    elif grade >= 70 :
        return f"your grade is {grade}, {name}--> you get C"
    elif grade >= 60 :
        return f"your grade is {grade}, {name}--> you get D"
    else:
        return f"your grade is {grade}, {name}--> you get F"

@app.route('/exercise3', methods=['POST'])
def exercise3_post():
    value = float(request.form['value'])
    if value < 0:
        return "invalid input, less than 0"
    return str(sqrt(value))

@app.route('/exercise4', methods=['POST'])
def exercise4_post():
    number = int(request.form['number'])
    count = 1
    random_num = randint(1, 100)
    while random_num != number:
        random_num = randint(1,100)
        count+=1
    return f'it took {count} iterations to match {number}'

@app.route('/exercise5', methods=['POST'])
def exercise5_post():
    number = int(request.form['number'])
    if(number <= 0):
        return f"{number}: error... limit is <=0"
    message = f"{number}: 0, 1, "
    first_num = 0
    second_num = 1
    while True:
        current_num = first_num + second_num
        if current_num > number:
            break
        message = message + f"{current_num}, "
        first_num = second_num
        second_num = current_num
    return message
